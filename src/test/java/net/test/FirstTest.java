package net.test;

import com.test.actions.Actions;
import com.test.base.BaseTest;
import com.test.pages.Pages;
import com.test.util.Constants;
import org.testng.Assert;
import org.testng.annotations.Test;

public class FirstTest extends BaseTest {
    private String query = "Java";

    @Test
    public void openGooglePage() {
        driver.get(Constants.BASE_URL);
        Actions.googleActions().search(query);

        for(String result : Actions.googleActions().getSearchResults()) {
            if(!result.contains(query)) {
                Assert.fail();
            }
        }
        Assert.assertTrue(true);
    }
}
