package com.test.actions;

import com.test.base.BaseActions;
import com.test.pages.Pages;

import java.util.List;

public class GoogleActions extends BaseActions {
    public void search(String query) {
        Pages.googlePage().insertIntoQueryField(query);
        Pages.googlePage().submitSearch();
    }

    public List<String> getSearchResults() {
        return Pages.googleResultsPage().getSearchResults();
    }
}
