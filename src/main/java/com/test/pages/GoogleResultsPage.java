package com.test.pages;

import com.test.base.BasePage;
import com.test.locators.Locator;
import com.test.locators.XPath;

import java.util.List;
import java.util.stream.Collectors;

public class GoogleResultsPage extends BasePage {
    Locator resultTitle = new XPath("//div[@class='rc']//h3");

    public List<String> getSearchResults() {
        return getElements(resultTitle).stream().map(r -> r.getText()).collect(Collectors.toList());
    }
}
