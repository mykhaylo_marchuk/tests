package com.test.pages;

import com.test.base.BasePage;
import com.test.locators.Locator;
import com.test.locators.XPath;

public class GooglePage extends BasePage {
    Locator searchField = new XPath("//input[@name='q']");
    Locator submitButton = new XPath("//input[@type='submit']");

    public void insertIntoQueryField(String query){
        waitForElementVisibility(searchField);
        type(String.format("Inserted %s into search field", query), query, searchField);
    }

    public void submitSearch() {
        waitForElementToBeClickable(submitButton);
        click("Submit button is clicked", submitButton);
    }
}
