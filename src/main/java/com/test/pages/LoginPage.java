package com.test.pages;


import com.test.base.BasePage;
import com.test.locators.CSS;
import com.test.locators.ID;
import com.test.locators.Locator;

public class LoginPage extends BasePage {
    Locator loginField = new ID("loginField");

    public boolean setLogin(String login) {
        waitForElementVisibility(loginField);
        type("Set login" + login + " to login field", login, loginField);

        return isElementPresent(loginField, login);
    }

    public void clickLoginButton() {
        waitForElementToBeClickable(loginField);
        click("Click login button", loginField);
    }
}
