package com.test.pages;

import com.test.actions.GoogleActions;

public class Pages {

    private static LoginPage loginPage;
    private static GooglePage googlePage;
    private static GoogleResultsPage googleResultsPage;

    public static LoginPage loginPage() {
        if (loginPage == null){
            loginPage = new LoginPage();
        }
        return loginPage;
    }

    public static GooglePage googlePage() {
        if(googlePage == null) {
            googlePage = new GooglePage();
        }
        return googlePage;
    }

    public static GoogleResultsPage googleResultsPage() {
        if(googleResultsPage == null) {
            googleResultsPage = new GoogleResultsPage();
        }
        return googleResultsPage;
    }
}
